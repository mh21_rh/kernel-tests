#!/bin/bash

# based on /usr/share/restraint/plugins/task_run.d/25_environment
. /usr/share/restraint/plugins/helpers
. /usr/share/restraint/plugins/cki_helpers

# Don't run from PLUGINS
if [ -n "$RSTRNT_NOPLUGINS" ]; then
    exec "$@"
    exit 0
fi

rstrnt_info "*** Running Plugin: $0"

if [[ -e "${CURRENT_TASK_PATH}" ]]; then
    # Make sure we don't try to run another task if system is already running one.
    # Workaround for https://gitlab.com/cki-project/upt/-/issues/99
    # We can't compare with RSTRNT_TASKID, because the reserve job that UPT uses to reserve a beaker machine
    # also runs the plugins, at least the recipe ID from UPT runs and the reservation is the same
    CURRENT_RECIPE_ID=$(ls ${CURRENT_TASK_PATH})
    if [[ -n "${CURRENT_RECIPE_ID}" ]] && [[ "${CURRENT_RECIPE_ID}" != "${RSTRNT_RECIPEID}" ]]; then
        rstrnt-report-result "${RSTRNT_TASKNAME}" FAIL
        echo "Aborting task $RSTRNT_TASKID from recipe ${RSTRNT_RECIPEID} as system expects to run tests for recipe ${CURRENT_RECIPE_ID}"
        exit 0
    fi
fi

# save information about running task
mkdir -p "${CURRENT_TASK_PATH}/${RSTRNT_RECIPEID}/${RSTRNT_TASKID}"
# saves the boot information everytime the task starts or continues
who -b >> "${CURRENT_TASK_PATH}/${RSTRNT_RECIPEID}/${RSTRNT_TASKID}/bootinfo"
exec "$@"
