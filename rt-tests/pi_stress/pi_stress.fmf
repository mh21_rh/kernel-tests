summary: Test priority inheritance mutexes
description: |
    Test priority inheritance mutexes

    The pi_stress program is a stress test that is intended to exercise
    kernel and C library code paths for POSIX mutexes using the Priority
    Inheritance attribute (PTHREAD_PRIO_INHERIT).

    The program consists of an admin thread (main), a reporter thread and
    some number of groups of three threads called "inversion
    groups". These thread groups are called that because they cause a
    condition called Priority Inversion, where a high priority thread is
    blocked due to a low-priority thread holding a shared
    resource. Priority inversion with no contravening logic is a deadlock
    condition.

    Each inversion group consists of three threads:

    1. A high-priority thread
    2. A medium-priority thread
    3. A low-priority thread

    The threads run through a state machine designed to guarantee that a
    low-priority thread holds a mutex while a medium priority thread runs
    (keeping the low-priority thread from releasing the mutex). The
    high-priority thread attempts to acquire the mutex and is blocked
    because of the low-priority thread holding it. If priority inheritence
    is working, the low-priority thread will receive a priority boost
    (will inherit the high-priority thread's priority) and will then run
    and release the mutex, averting a deadlock.

    On a multi-processor system, the admin and reporter threads are run
    one one processor while the inversion groups are run on another
    processor.

    Utilities:
        pi_stress - A program used to stress test POSIX Priority Inheritance mutexes.
        pip_stress - A program used to test priority inheritance between processes.

    Test Inputs:
        $PARAM_SEC, defaults to '30'.
        $PARAM_GROUPS, defaults to '1'.

        For each execution of pi_stress, expect the configured deadlock of each group to
        be handled through priority inheritance mutexes and result in an inversion
        occurring with no reported errors.
            Execute 'pi_stress --quiet --duration=$PARAM_SEC --groups=$PARAM_GROUPS'.
            Execute 'pi_stress --quiet --duration=$PARAM_SEC --groups=$PARAM_GROUPS --rr'.
            Execute 'pi_stress --quiet --groups=$(( nrcpus )) --duration=30'.

        Execute 'pip_stress' and expect a priority inversion to be handled through
        priority inheritance.

    Expected result:
        pi_stress:
            For each execution, no error message entries in the log,
            and a summary message indicating that inversions occurred.
                Total inversion performed: xxx

        pip_stress:
            Successfully used priority inheritance to handle an inversion

    Results location:
        output.txt | taskout.log, log is dependent upon the test executor.

    STEPS TO RUN
    ./runtest.sh
contact: Waylon Cude <wcude@redhat.com>
path: /rt-tests/pi_stress
test: bash ./runtest.sh
framework: shell
require:
    - realtime-tests
    - type: file
      pattern:
          - /rt-tests/include
          - /cki_lib
          - /automotive/include
duration: 10m
extra-summary: /rt-tests/pi_stress
extra-task: /rt-tests/pi_stress
environment:
    PARAM_SEC: 30
    PARAM_GROUPS: 1
id: 2512d602-6f29-4377-a6f1-150ba80ac4e3
